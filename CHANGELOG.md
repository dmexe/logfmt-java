# Changelog

## [0.1.1] - 2020-08-14

### Added

- LogfmtLayout: log thread name

## [0.1.0] - 2020-08-13

### Added

- Initial release