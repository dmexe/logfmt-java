package com.gitlab.dmexe.logfmt;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Extracts a key/value pairs from logfmt encoded string.
 */
public class LogfmtDecoder {
    private final CharSequence string;

    /**
     * Creates a new instance.
     */
    public LogfmtDecoder(CharSequence string) {
        Objects.requireNonNull(string, "string cannot be null");

        this.string = string;
    }

    /**
     * Extracts a key/value pairs from logfmt encoded string.
     */
    public Map<String, String> read() {
        Map<String, String> pairs = new HashMap<>();
        CharSequence string = trimStart();

        if (string.length() == 0) {
            return pairs;
        }

        read(string, pairs);
        return pairs;
    }


    /**
     * Extracts a key/value pairs from logfmt encoded string into destination map.
     */
    private void read(CharSequence string, Map<String, String> dst) {
        String key       = "";
        String value     = "";

        boolean inKey    = false;
        boolean inValue  = false;
        boolean inQuote  = false;
        boolean hadQuote = false;

        for(int i = 0; i <= string.length() ; i++) {
            if (i == string.length() || (string.charAt(i) == ' ' && !inQuote)) {
                if (inKey && key.length() > 0) {
                    dst.put(key, "true");
                } else if (inValue) {
                    if (value.equals("true")) {
                        dst.put(key, "true");
                    } else if (value.equals("false")) {
                        dst.put(key, "false");
                    } else if (value.equals("") && !hadQuote) {
                        dst.put(key, "");
                    } else {
                        dst.put(key, value);
                    }
                    value = "";
                }

                inKey = false;
                inValue = false;
                inQuote = false;
                hadQuote = false;
            }

            if (i < string.length()) {
                if (string.charAt(i) == '=' && !inQuote) {
                    //println("split")
                    inKey = false;
                    inValue = true;
                } else if (string.charAt(i) == '\\') {
                    i++;
                    value = value + string.charAt(i);
                    //println("escape: " + line(i))
                } else if (string.charAt(i) == '"') {
                    hadQuote = true;
                    inQuote = !inQuote;
                    //println("in quote: " + inQuote)
                } else if (string.charAt(i) != ' ' && !inValue && !inKey) {
                    //println("start key with: " + line(i))
                    inKey = true;
                    key = "" + string.charAt(i);
                } else if (inKey) {
                    //println("add to key: " + line(i))
                    key = key + string.charAt(i);
                } else if (inValue) {
                    //println("add to value: " + line(i))
                    value = value + string.charAt(i);
                }
            }
        }

    }

    /**
     * Remove whitespace symbols from beginning of string.
     */
    private CharSequence trimStart() {
        int index = 0;

        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) != ' ') {
                break;
            }
            index = i;
        }

        return string.subSequence(index, string.length());
    }
}
