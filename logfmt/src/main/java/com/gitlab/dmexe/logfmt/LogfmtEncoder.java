package com.gitlab.dmexe.logfmt;

import java.io.IOException;
import java.io.Writer;
import java.util.Objects;

/**
 * Formats key/value pairs as logfmt entries and writes its into inner writer.
 */
public class LogfmtEncoder {
    private static final char QUOTE = '"';
    private static final char EQUAL = '=';
    private static final char SPACE = ' ';

    private final Writer writer;

    /**
     * Creates a new instance of encoder.
     *
     * @param writer is an inner writer object.
     */
    public LogfmtEncoder(Writer writer) {
        Objects.requireNonNull(writer, "writer cannot be null");
        this.writer = writer;
    }

    /**
     * Formats and writes key/pair values into inner writer.
     */
    public LogfmtEncoder write(String key, String value) throws IOException {
        Objects.requireNonNull(key, "key cannot be null");

        writer.write(key);
        writer.write(EQUAL);
        writeValue(value);

        return this;
    }

    /**
     * Writes a delimiter for the next key/value entry.
     */
    public LogfmtEncoder next() throws IOException {
        writer.write(SPACE);
        return this;
    }

    /**
     * Writes a new line separator.
     */
    public void finish() throws IOException {
        writer.write("\n");
    }

    /**
     * Writes a value (quoted or unquoted).
     */
    private void writeValue(String value) throws IOException {
        if (value == null || value.length() == 0) {
            writer.write(QUOTE);
            writer.write(QUOTE);
            return;
        }

        if (needsQuoting(value)) {
            writeQuotedValue(value);
            return;
        }

        writer.write(value);
    }

    /**
     * Escapes and writes a value which needs quoting.
     */
    private void writeQuotedValue(String value) throws IOException {
        writer.write(QUOTE);

        char c;
        for (int i = 0; i < value.length(); i++ ) {
            c = value.charAt(i);
            switch (c) {
                case '\\':
                case '"':
                    writer.write('\\');
                    writer.write(c);
                    break;
                case '\r':
                    break;
                case '\n':
                    writer.write(SPACE);
                    break;
                default:
                    writer.write(c);
            }
        }

        writer.write(QUOTE);
    }

    /**
     * Detects if given string needs quoting.
     *
     * Any strings which contains only "a-Z0-9-." are unquoted.
     */
    private static boolean needsQuoting(String string) {
        int  i;
        char ch;
        for (i = 0 ; i < string.length(); i ++) {
            ch = string.charAt(i);

            if (!(
                    (ch >= 'a' && ch <= 'z') ||
                            (ch >= 'A' && ch <= 'Z') ||
                            (ch >= '0' && ch <= '9') ||
                            (ch == '-' || ch == '.')
            )) {
                return true;
            }
        }
        return false;
    }
}
