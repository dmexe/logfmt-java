package com.gitlab.dmexe.logfmt;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class LogfmtEncoderTest {
    @Test
    void writeUnquotedKeyValue() throws IOException {
        String line = encode(enc -> enc.write("key", "Value09-."));

        assertThat(line).isEqualTo("key=Value09-.");
    }

    @Test
    void writeQuotedValue() throws IOException {
        String line = encode(enc -> enc.write("key", "needs \"quoting"));

        assertThat(line).isEqualTo("key=\"needs \\\"quoting\"");
    }

    @Test
    void replacesNewLineSymbol() throws IOException {
        String line = encode(enc -> enc.write("key", "foo\r\nbar"));

        assertThat(line).isEqualTo("key=\"foo bar\"");
    }

    @Test
    void writesMultipleEntries() throws IOException {
        String line = encode(enc -> enc
                .write("1st", "foo")
                .next()
                .write("2nd", "bar")
                .next()
                .write("3rd", "baz")
        );

        assertThat(line).isEqualTo("1st=foo 2nd=bar 3rd=baz");
    }

    @Test
    void writesNewLine() throws IOException {
        String line = encode(enc -> enc.write("foo", "bar").finish());
        assertThat(line).isEqualTo("foo=bar\n");
    }

    @Test
    void writeNullValue() throws IOException {
        String line = encode(enc -> enc.write("key", null) );

        assertThat(line).isEqualTo("key=\"\"");
    }

    @Test
    void writeEmptyValue() throws IOException {
        String line = encode(enc -> enc.write("key", "") );

        assertThat(line).isEqualTo("key=\"\"");
    }

    @Test
    void keyCannotBeNull() {
        assertThatThrownBy(() -> encode(enc -> enc.write(null, "foo")))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("key cannot be null");
    }

    @Test
    void writerCannotBeNull() {
        assertThatThrownBy(() -> new LogfmtEncoder(null))
                .isInstanceOf(NullPointerException.class)
                .hasMessage("writer cannot be null");
    }

    private String encode(CheckedConsumer<LogfmtEncoder> func) throws IOException {
        Writer writer = new StringWriter();
        LogfmtEncoder enc = new LogfmtEncoder(writer);

        func.accept(enc);

        return writer.toString();
    }

    @FunctionalInterface
    interface CheckedConsumer<T> {
        void accept(T value) throws IOException;
    }
}