package com.gitlab.dmexe.logfmt;

import java.util.AbstractMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class LogfmtDecoderTest {
  @Test
  void stringValue() {
    assertThat(read("hello")).containsOnly(entry("hello", "true"));
  }

  @Test
  void keyValuePair() {
    assertThat(read("foo=bar")).containsOnly(entry("foo", "bar"));
  }

  @Test
  void booleanValues() {
    assertThat(read("foo=true bar=false")).containsOnly(
        entry("foo", "true"),
        entry("bar", "false")
    );
  }

  @Test
  void quotedValues() {
    assertThat(read("hello=\"'kitty'\" second='kitty' last=\"one \\\"two\\\"\"")).containsOnly(
        entry("hello", "'kitty'"),
        entry("second", "'kitty'"),
        entry("last", "one \"two\"")
    );
  }

  @Test
  void specialSymbolValues() {
    assertThat(read("foo=\"hello=kitty\"")).containsOnly(
        entry("foo", "hello=kitty")
    );
  }

  @Test
  void complexValue() {
    String source = "foo=bar a=14 baz=\"hello kitty\""
        + " cool%story=bro f %^asdf code=H12 path=/hello/user@foo.com/close";
    assertThat(read(source)).containsOnly(
        entry("foo", "bar"),
        entry("a", "14"),
        entry("baz", "hello kitty"),
        entry("cool%story", "bro"),
        entry("f", "true"),
        entry("%^asdf", "true"),
        entry("code", "H12"),
        entry("path", "/hello/user@foo.com/close")
    );
  }

  @Test
  void trimString() {
    assertThat(read("  foo=bar  ")).containsExactly(entry("foo", "bar"));
  }

  @Test
  void emptySource() {
    assertThat(read("")).isEmpty();
    assertThat(read(" ")).isEmpty();
  }

  @Test
  void emptyValue() {
    assertThat(read("foo= bar=\"\"")).containsOnly(
        entry("foo", ""),
        entry("bar", "")
    );
  }

  private static Map<String, String> read(String string) {
    return new LogfmtDecoder(string).read();
  }

  private static <K, V> Map.Entry<K, V> entry(K key, V value) {
    return new AbstractMap.SimpleEntry<>(key, value);
  }
}