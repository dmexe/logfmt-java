import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.gradle.api.publish.maven.MavenPom

plugins {
    `java-library`
    `maven-publish`

    id("com.github.johnrengelman.shadow")
    id("com.jfrog.bintray")
}

java {
    withSourcesJar()
}

dependencies {
    implementation(project(":logfmt"))
    implementation("org.apache.logging.log4j:log4j-core:2.13.3")
}

tasks {
    named<ShadowJar>("shadowJar") {
        classifier = ""

        dependencies {
            exclude {
                it.moduleGroup == "org.apache.logging.log4j"
            }
        }
    }
}

fun MavenPom.addDependencies() = withXml {
    asNode().appendNode("dependencies").let { depNode ->
        configurations.implementation.get().allDependencies.forEach {
            if (it.name != "logfmt") {
                depNode.appendNode("dependency").apply {
                    appendNode("groupId", it.group)
                    appendNode("artifactId", it.name)
                    appendNode("version", it.version)
                }
            }
        }
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = project.name
            version = project.version.toString()

            artifact(tasks["sourcesJar"])
            artifact(tasks["shadowJar"])

            pom.addDependencies()
        }
    }
}

bintray {
    user = System.getenv("BINTRAY_USER")
    key = System.getenv("BINTRAY_KEY")

    setPublications("maven")

    pkg.apply {
        repo = "maven"
        name = project.name
        vcsUrl = "https://gitlab.com/dmexe/logfmt-java.git"
        websiteUrl = "https://gitlab.com/dmexe/logfmt"
        description = "The logfmt layout for Log4j V2 logging library"
        setLicenses("Apache-2.0")

        version.name = project.version.toString()
        version.vcsTag = project.version.toString()
    }
}
