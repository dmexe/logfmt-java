package com.gitlab.dmexe.logfmt.layouts;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.core.impl.Log4jLogEvent;
import org.apache.logging.log4j.message.FormattedMessage;
import org.apache.logging.log4j.util.SortedArrayStringMap;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;

class LogfmtLayoutTest {
    @Test
    void logMessage() {
        String line = write(b -> { });

        assertThat(line)
            .contains("level=info ")
            .contains(" time=\"")
            .contains(" msg=ping ")
            .contains(" thread=\"" + Thread.currentThread().getName() + "\" ")
            .contains(" logger=LogfmtLayoutTest ")
            .contains(" source=\"class.method(file:99)\"\n");
    }

    @Test
    void logMessageWithoutLocation() {
        String line = write(b -> b.setIncludeLocation(false));

        assertThat(line).doesNotContain("source=");
    }

    @Test
    void logMessageWithMarker() {
        String line = write(b -> b.setMarker(MarkerManager.getMarker("MARKER")));

        assertThat(line).contains(" marker=MARKER ");
    }

    @Test
    void logMessageWithoutLoggerName() {
        String line = write(b -> b.setLoggerName(null));

        assertThat(line).doesNotContain("logger=");
    }

    @Test
    void logMessageWithThrowable() {
        Throwable err = new RuntimeException("boom");
        String line = write(b -> b.setThrown(err));

        assertThat(line).contains(" err=\"java.lang.RuntimeException: boom\" ");
    }
    @Test
    void logMessageWithMDC() {
        Map<String,String> mdc = new HashMap<>();
        mdc.put("foo", "bar");
        mdc.put("hello", "kitty");
        String line = write(b -> b.setContextData(new SortedArrayStringMap(mdc)));

        assertThat(line).contains(" foo=bar hello=kitty\n");
    }

    private static String write(Consumer<Log4jLogEvent.Builder> cfg) {
        LogfmtLayout layout = createLayout();
        Log4jLogEvent.Builder builder = createLogBuilder();
        cfg.accept(builder);

        return layout.toSerializable(builder.build());
    }

    private static Log4jLogEvent.Builder createLogBuilder() {
        return new Log4jLogEvent.Builder()
            .setLevel(Level.INFO)
            .setLoggerName("LogfmtLayoutTest")
            .setTimeMillis(100)
            .setIncludeLocation(true)
            .setSource(new StackTraceElement(
                "class",
                "method",
                "file",
                99)
            )
            .setMessage(new FormattedMessage("ping"));
    }

    private static LogfmtLayout createLayout() {
        return new LogfmtLayout(
            StandardCharsets.UTF_8,
            true,
            true,
            true,
            true
        );
    }
}