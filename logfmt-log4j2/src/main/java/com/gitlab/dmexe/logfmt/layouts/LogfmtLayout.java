package com.gitlab.dmexe.logfmt.layouts;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.Map;

import com.gitlab.dmexe.logfmt.LogfmtEncoder;
import org.apache.logging.log4j.LoggingException;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.Node;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.AbstractStringLayout;

@Plugin(
    name = "LogfmtLayout",
    category = Node.CATEGORY,
    elementType = Layout.ELEMENT_TYPE,
    printObject = true
)
public class LogfmtLayout extends AbstractStringLayout {
    private static final String LEVEL_KEY = "level";
    private static final String TIME_KEY = "time";
    private static final String MESSAGE_KEY = "msg";
    private static final String THREAD_KEY = "thread";
    private static final String LOGGER_KEY = "logger";
    private static final String SOURCE_KEY = "source";
    private static final String MARKER_KEY = "marker";
    private static final String ERROR_KEY = "err";

    private final boolean includeTime;
    private final boolean includeLocation;
    private final boolean includeMarker;
    private final boolean includeError;

    protected LogfmtLayout(
        final Charset charset,
        final boolean includeTime,
        final boolean includeLocation,
        final boolean includeMarker,
        final boolean includeError
    ) {
        super(charset);
        this.includeTime = includeTime;
        this.includeLocation = includeLocation;
        this.includeMarker = includeMarker;
        this.includeError = includeError;
    }

    /**
     * Creates a layout instance.
     */
    @PluginFactory
    public static LogfmtLayout createLayout(
        @PluginAttribute(value = "charset", defaultString = "UTF-8") final Charset charset,
        @PluginAttribute(value = "includeTime", defaultBoolean = true) final boolean includeTime,
        @PluginAttribute(value = "includeLocation") final boolean includeLocation,
        @PluginAttribute(value = "includeMarker") final boolean includeMarker,
        @PluginAttribute(value = "includeError", defaultBoolean = true) final boolean includeError
    ) {
        return new LogfmtLayout(
            charset,
            includeTime,
            includeLocation,
            includeMarker,
            includeError
        );
    }

    @Override
    public String toSerializable(final LogEvent event) {
        final Writer writer = new StringWriter();
        final LogfmtEncoder logfmt = new LogfmtEncoder(writer);

        try {
            write(event, logfmt);
        } catch (IOException err) {
            throw new LoggingException("Logfmt write error", err);
        }

        return writer.toString();
    }

    private void write(final LogEvent event, final LogfmtEncoder logfmt) throws IOException {
        logfmt.write(LEVEL_KEY, event.getLevel().name().toLowerCase());

        if (includeTime) {
            final Instant time = Instant.ofEpochMilli(event.getInstant().getEpochMillisecond());
            logfmt.next().write(TIME_KEY, time.toString());
        }

        logfmt.next().write(MESSAGE_KEY, event.getMessage().getFormattedMessage());

        if (includeMarker && event.getMarker() != null) {
            logfmt.next().write(MARKER_KEY, event.getMarker().toString());
        }

        if (includeError && event.getThrown() != null) {
            logfmt.next().write(ERROR_KEY, event.getThrown().toString());
        }

        if (event.getThreadName() != null) {
            logfmt.next().write(THREAD_KEY, event.getThreadName());
        }

        if (event.getLoggerName() != null) {
            logfmt.next().write(LOGGER_KEY, event.getLoggerName());
        }

        if (includeLocation && event.isIncludeLocation()) {
            final StackTraceElement source = event.getSource();
            if (source != null) {
                logfmt.next().write(SOURCE_KEY, source.toString());
            }
        }

        final Map<String, String> mdc = event.getContextData().toMap();
        if (mdc != null && !mdc.isEmpty()) {
            for (final Map.Entry<String, String> entry : mdc.entrySet()) {
                logfmt.next().write(entry.getKey(), entry.getValue());
            }
        }

        logfmt.finish();
    }
}
