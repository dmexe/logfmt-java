package com.gitlab.dmexe.logfmt.examples;

import org.apache.logging.log4j.CloseableThreadContext;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.apache.logging.log4j.message.MapMessage;

import java.util.HashMap;
import java.util.Map;

public class Log4j2Example {
    private final static Logger logger = LogManager.getLogger(Log4j2Example.class);

    public static void main(String[] args) {
        logger.info("Hello {}", "World");

        Map<String, String> kv = new HashMap<>();
        kv.put("foo", "bar");
        kv.put("hello", "kitty");
        kv.put("message", "map test");
        logger.debug(new MapMessage<>(kv));

        Marker marker = MarkerManager.getMarker("PING");
        logger.warn(marker, "a marker test");

        try(CloseableThreadContext.Instance _mdc = CloseableThreadContext.put("foo", "bar").put("hello", "kitty")) {
            logger.info("MDC context test");
        }

        try {
            throw new RuntimeException("boom");
        } catch (RuntimeException err) {
            logger.error(err);
            logger.error("An error test", err);
        }
    }
}
