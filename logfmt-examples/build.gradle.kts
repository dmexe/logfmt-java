plugins {
    `application`
}

repositories {
    jcenter()
}

dependencies {
    //implementation(project(":logfmt-log4j2"))
    implementation("com.gitlab.dmexe:logfmt-log4j2:0.1.1")
    implementation("org.apache.logging.log4j:log4j-api:2.13.3")
    implementation("org.apache.logging.log4j:log4j-core:2.13.3")
}
