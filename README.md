# logfmt-java

[ ![Download](https://api.bintray.com/packages/dmexe/maven/logfmt-log4j2/images/download.svg) ](https://bintray.com/dmexe/maven/logfmt-log4j2/_latestVersion)


Implements encoder, decoder and [log4j2][log4j2] Layout for [logfmt][logfmt]
key value logging convention.  

## Package structure

* [logfmt](./logfmt) - encoder and decoder implementations for [logfmt][logfmt]
* [logfmt-log4j2](./logfmt-log4j2) - [log4j2][log4j2] Layout implementation
* [logfmt-examples](./logfmt-examples) - [log4j2][log4j2] usage example

## Log4j2 Layout usage

Including it in your project, add to `build.gradle`

```groovy
repositories {
    jcenter()
}

dependencies {
    implementation("com.gitlab.dmexe:logfmt-log4j2:0.1.1")
    implementation("org.apache.logging.log4j:log4j-api:2.13.3")
    implementation("org.apache.logging.log4j:log4j-core:2.13.3")
}
```

Create the configuration file `log4j2.xml`

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration status="WARN" packages="com.gitlab.dmexe.logfmt.layouts">
    <Appenders>
        <Console name="Console" target="SYSTEM_OUT">
            <LogfmtLayout />
        </Console>
    </Appenders>
    <Loggers>
        <Root level="debug">
            <AppenderRef ref="Console"/>
        </Root>
    </Loggers>
</Configuration>
```

The `LogfmtLayout` configuration parameters

| Name | Default Value | Comment |
|------|---------------|---------|
| charset | UTF-8 | The encoding charset for log messages |
| includeTime | true | enable or disable the "time" field logging |
| includeLocation | false | enable or disable the "source" field logging |
| includeMarker | false | enable or disable the "marker" field logging |
| includeError | true | enable or disable the "err" field logging |

Usage example:

```java
logger.info("Hello world");
logger.error("An unexpected error", cause);
try(CloseableThreadContext.Instance _mdc = CloseableThreadContext.put("foo", "bar")) {
    logger.info("MDC context");
}

/**
level=info time="2020-08-12T17:54:54.935Z" msg="Hello World" thread=main logger=com.gitlab.dmexe.logfmt.examples.Log4j2Example
level=error time="2020-08-12T17:54:54.999Z" msg="An unexpected error" thread=main err="java.lang.RuntimeException: boom" logger=com.gitlab.dmexe.logfmt.examples.Log4j2Example
level=info time="2020-08-12T17:57:41.534Z" msg="MDC context" thread=main logger=com.gitlab.dmexe.logfmt.examples.Log4j2Example foo=bar
 **/
```

[logfmt]: https://brandur.org/logfmt
[log4j2]: https://logging.apache.org/log4j/2.x/index.html