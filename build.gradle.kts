plugins {
    java
    id("com.github.johnrengelman.shadow") version "6.0.0" apply false
    id("com.jfrog.bintray") version "1.8.5" apply false
}

allprojects {
    group = "com.gitlab.dmexe"
    version = "0.1.1"

    repositories { jcenter() }
}

subprojects {
    afterEvaluate {
        java {
            sourceCompatibility = JavaVersion.VERSION_1_8
            targetCompatibility = JavaVersion.VERSION_1_8
        }
    }

    if (arrayOf("logfmt-log4j2").contains(name)) {
        apply(plugin = "com.github.johnrengelman.shadow")
        apply(plugin = "com.jfrog.bintray")
    }

    if (arrayOf("logfmt", "logfmt-log4j2").contains(name)) {
        tasks.withType<Test> {
            useJUnitPlatform()

            testLogging {
                events("PASSED", "FAILED", "SKIPPED")
                showStandardStreams = true
            }
        }

        afterEvaluate {
            dependencies {
                testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
                testImplementation("org.assertj:assertj-core:3.16.1")
                testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.2")
            }
        }
    }
}
